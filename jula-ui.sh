#!/usr/bin/bash
session="jula-ui"
doc1="~/dev/jula-ui/src/css/vars.css"
doc2="~/dev/jula-ui/src/partials/header-content.hbs"
doc3="~/dev/jula-ui/src/css/header-powerline.css"

tmux start-server
tmux new-session -d -s $session  
tmux split-window -h
tmux split-window -v
tmux selectp -t 1 
tmux send-keys "pushd `dirname $doc1`" C-m
tmux send-keys "nvim ${doc1##*/}" C-m
tmux selectp -t 0 
tmux send-keys "pushd ~/dev/jula-ui" C-m
tmux send-keys "nvim -S jula-ui.session" C-m
tmux selectp -t 2 
tmux send-keys "gulp bundle && gulp preview" C-m
tmux new-window -t $session:1 -n scratch
tmux select-window -t $session:0
tmux attach-session -t $session
